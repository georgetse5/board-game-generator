## Generator Menu
✅   - Create game schema <br>
✅	 	- Reads the json and extract the game info <br>
✅	  	- Add Description section for every game into json file <br>
✅   - Class Selector for json data retrieve <br>
✅ -  Class Generator <br>

## Games
🚧 <b><u>Snakes and Ladders</u></b>

🚧	- Take the game info-description from generator class <br>
✅	- Generates the game <br>
✅	- Game's board <br>
✅	- Start game method <br>
🚧	- getDice method <br>
🚧	- Checks if the player is on top of a snake <br>
🚧	- Save game	<br>
🚧	- Load game <br>
    