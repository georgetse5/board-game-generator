package gen.board.game;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SaveGame {

        public void saveGame(String fileName, List<String> playerNames, List<Integer> playerPositions) {
            JSONObject gameData = new JSONObject();
            JSONArray playerData = new JSONArray();


            for (int i = 0; i < playerNames.size(); i++) {
                JSONObject player = new JSONObject();
                player.put("name", playerNames.get(i));
                player.put("position", playerPositions.get(i));
                playerData.add(player);
            }


            gameData.put("players", playerData);


            try (FileWriter file = new FileWriter(fileName)) {
                file.write(gameData.toJSONString());
                System.out.println("Game data saved to file: " + fileName);
            } catch (IOException e) {
                System.out.println("Error saving game data to file: " + fileName);
                e.printStackTrace();
            }
        }


    public void loadGame(String fileName, List<String> playerNames, List<Integer> playerPositions) {

        playerNames.clear();
        playerPositions.clear();

        try {
            Object obj = new JSONParser().parse(new FileReader(fileName));
            JSONObject gameData = (JSONObject) obj;
            JSONArray playerData = (JSONArray) gameData.get("players");


            for (int i = 0; i < playerData.size(); i++) {
                JSONObject player = (JSONObject) playerData.get(i);
                playerNames.add((String) player.get("name"));
                playerPositions.add(((Long) player.get("position")).intValue());
            }

            System.out.println("Game data loaded from file: " + fileName);
        } catch (IOException | ParseException e) {
            System.out.println("Error loading game data from file: " + fileName);
            e.printStackTrace();
        }
    }

}
