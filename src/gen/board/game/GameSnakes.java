package gen.board.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.List;

public class GameSnakes {

    List<String> playerNames;
    List<Integer> playerPosition = new ArrayList<Integer>();
    int boardSize;
    SaveGame saveGame = new SaveGame();
    String fileName = "gameSave.json";

    public GameSnakes(List<String> playerNames, int boardSize) {
        this.playerNames = playerNames;
        this.boardSize = boardSize;
    }

    public void board() throws IOException {

        System.out.println("\t\t\t\t\t\t\t\t\u001B[36m\u001B[40mSnakes And Ladders\u001B[0m");
        System.out.println("\u001B[36m\u001B[40m|\t-----------------------------------------------------------------------------------\t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΤΟ ΦΙΔΑΚΙ ΕΙΝΑΙ ΕΝΑ ΕΠΙΤΡΑΠΕΖΙΟ ΠΑΙΧΝΙΔΙ ΤΟ ΟΠΟΙΟ ΠΑΙΖΕΤΑΙ ΑΠΟ 2 ΕΩΣ 4 ΠΑΙΚΤΕΣ  \t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΣΚΟΠΟΣ ΤΟΥ ΠΑΙΧΝΙΔΙΟΥ ΕΙΝΑΙ ΝΑ ΦΤΑΣΕΙ ΚΑΠΟΙΟΣ ΣΤΟ 100. ΣΕ ΔΙΑΦΟΡΑ ΣΗΜΕΙΑ ΤΟΥ    \t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΠΑΙΧΝΙΔΙΟΥ ΥΠΑΡΧΟΥΝ ΠΑΓΙΔΕΣ ΟΠΩΣ ΕΙΝΑΙ ΤΟ ΣΤΟΜΑ ΤΟΥ ΦΙΔΙΟΥ ΤΟ ΟΠΟΙΟ ΚΑΤΑΠΙΝΕΙ   \t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΤΟΝ ΠΑΙΚΤΗ ΠΟΥ ΘΑ ΠΕΣΕΙ ΕΠΑΝΩ ΤΟΥ ΡΙΧΝΟΝΤΑΣ ΤΟΝ ΣΕ ΜΙΚΡΟΤΕΡΗ ΘΕΣΗ. ΑΠΟ ΤΗΝ ΑΛΛΗ \t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΥΠΑΡΧΟΥΝ ΚΑΙ ΒΟΗΘΕΙΕΣ ΟΠΩΣ ΕΙΝΑΙ ΟΙ ΣΚΑΛΕΣ. ΕΑΝ ΚΑΠΟΙΟΣ ΠΕΣΕΙ ΕΠΑΝΩ ΣΕ ΜΙΑ ΣΚΑΛΑ\t|\u001B[0m");
        System.out.println("\u001B[40m|\t ΤΟΤΕ ΜΕΤΑΤΟΠΙΖΕΤΑΙ ΣΕ ΜΕΓΑΛΥΤΕΡΗ ΘΕΣΗ ΒΟΗΘΩΝΤΑΣ ΤΟΝ ΝΑ ΦΤΑΣΕΙ ΠΡΩΤΟΣ ΣΤΟ ΤΕΛΟΣ  \t|\u001B[0m");
        System.out.println("\u001B[36m\u001B[40m|\t-----------------------------------------------------------------------------------\t|\u001B[0m");


        System.out.println("\n# ==========================================\u001B[31m\u001B[40mBOARD\u001B[0m================================================ #\n");


        int cTiles = boardSize, iter = -1;

        while (cTiles > 0) {
            if (cTiles % 10 == 0 && cTiles != boardSize) {
                if (iter == -1) {
                    cTiles = cTiles - 9;
                    iter = 1;
                } else {
                    System.out.print(cTiles);
                    cTiles -= 10;
                    iter = -1;
                }
                if (cTiles != 0)
                    System.out.print("\n" + cTiles + "\t");
            } else
                System.out.print(cTiles + "\t");
            cTiles = cTiles + iter;
        }

        System.out.println("\n\n# =======================================\u001B[31m\u001B[40mPLAYER DATA\u001B[0m======================================== #\n");

// Some checks on player names and size
// # ================================================================================================================================ #

//        // testing for name parsing
//        System.out.println();
//        for (String name : playerNames) {
//            System.out.println("Player: " + name);
//        }
//        System.out.println("\n");
//
//
//        // Testing for number of players from list's size (length)
//        System.out.println();
//        int count = 0;
//        for (int i = 0; i < playerNames.size(); i++) {
//            count++;
//            System.out.println("Player " + count);
//        }
//        System.out.println("Number of Players: " + count);
//        System.out.println();

// # ================================================================================================================================ #

//        Scanner scanner = new Scanner(System.in);
        String roll = "";
//
//        while (!roll.equals("r") && !roll.equals("R") && !roll.equals("s")) {
//            System.out.print("Please enter 'r' to roll the dice, 's' to save the game, or 'R' to re-roll: ");
//            roll = scanner.nextLine();
//        }

        playGame(roll);
    }

// # ================================================================================================================================ #

    public String playGame(String roll) throws IOException {

        int pos, playerRoll, numberOfPlayers;
        String reRoll, pname;

        // -------------------------- //
        // Add players to player list //
        // -------------------------- //

        for (int i = 0; i < playerNames.size(); i++) {
            playerPosition.add(1);
        }

        numberOfPlayers = playerNames.size();

        int snakesLaddersArray[] = new int[8];
        snakesLaddersArray[0] = 54;
        snakesLaddersArray[1] = 90;
        snakesLaddersArray[2] = 99;
        snakesLaddersArray[3] = 9;
        snakesLaddersArray[4] = 40;
        snakesLaddersArray[5] = 67;
        snakesLaddersArray[6] = 24;
        snakesLaddersArray[7] = 64;


// # ================================================================================================================================ #

        reRoll = getInput();
        System.out.print("\n\n");

        while (reRoll.equals("r") || reRoll.equals("R") || reRoll.equals("s")) {
                int ploc;
                for (int i = 0; i < numberOfPlayers; i++) {
                    playerRoll = (int) (Math.random() * 6 + 1);


                    playerPosition.set(i, playerPosition.get(i) + playerRoll);
                    ploc = playerPosition.get(i);
                    pname = playerNames.get(i);

                    pos = checkPosition(ploc, pname, snakesLaddersArray);

                    playerPosition.set(i, pos);

                    if (playerPosition.get(i) > 100) {
                        playerPosition.set(i, 100);
                    }

                    System.out.println("-------------------------");
                    System.out.println("|\t" + "\033[36m" + playerNames.get(i) + "\033[0m" + " rolled " + "\033[31m" + playerRoll + "\033[0m" + "\t\t|");
                    System.out.println("|\t" + "At location " + "\033[36m" + playerPosition.get(i) + "\033[0m" + "\t|");
                    System.out.println("-------------------------");
//                    System.out.println("Check: " + playerPosition.get(i));
                }

                // # ================================================================================================================================ #


                int newPosition;
                boolean gameFinished = false;
                for (int i = 0; i < numberOfPlayers; i++) {

                    // ---------------------- //
                    // Update player position //
                    // ---------------------- //

                    newPosition = playerPosition.get(i);

                    if (newPosition <= 100) {
                        playerPosition.set(i, newPosition);
                    }

                    // ------------------------- //
                    // Check if game is finished //
                    // ------------------------- //

                    if (playerPosition.get(i) >= 100) {
                        if (playerPosition.get(i) > 100) {
                            playerPosition.set(i, 100);
                        }
                        gameFinished = true;
                        System.out.println("\n\n\n\033[36m" + playerNames.get(i) + "\033[0m" + " has \033[33mWON\033[0m the game!\n\n\n");
                        break;
                    }
                }

                // ------------- //
                // Game finished //
                // ------------- //

                if (gameFinished) {
                    System.out.print("Game has finished");
                    break;
                }

                // -------------- //
                // Game Continued //
                // -------------- //

                else {
                    reRoll = getInput();
                    System.out.print("\n\n");
                }

                // ------------- //
                // Save and Roll //
                // ------------- //

                if (reRoll.equals("s")) {
                    for (int it = 0; it < numberOfPlayers; it++) {
                        saveGame.saveGame(fileName, playerNames, playerPosition);
                    }
                }

                // ------------------ //
                // Save and Quit game //
                // ------------------ //

                if (reRoll.equals("sq")) {
                    for (int it = 0; it < numberOfPlayers; it++) {
                        saveGame.saveGame(fileName, playerNames, playerPosition);
                    }
                    break;
                }
        }
        return reRoll;
    }




    public int checkPosition(int playerPosition, String name, int snakesLaddersArray[]) throws IOException {

        int pos = 1;

            if (playerPosition == snakesLaddersArray[0]) // Snake 1
            {
                playerPosition = 19;
                System.out.println( "\u001B[31m" + name + " you foot on a snake, GO DOWN!!! \u001B[0m");
            } else if (playerPosition == snakesLaddersArray[1]) // Snake 2
            {
                playerPosition = 48;
                System.out.println("\u001B[31m" + name + " you foot on a snake, GO DOWN!!! \u001B[0m ");

            } else if (playerPosition == snakesLaddersArray[2]) // Snake 3
            {
                playerPosition = 77;
                System.out.println("\u001B[31m" + name + " you foot on a snake, GO DOWN!!! \u001B[0m ");
            } else if (playerPosition == snakesLaddersArray[3]) // Ladder 1
            {
                playerPosition = 34;
                System.out.println("\u001B[32m" + name + " you Found A Ladder!! GO UP!!! \u001B[0m ");

            } else if (playerPosition == snakesLaddersArray[4]) // Ladder 2
            {
                playerPosition = 64;
                System.out.println("\u001B[32m" + name + " you Found A Ladder!! GO UP!!! \u001B[0m");

            } else if (playerPosition == snakesLaddersArray[5]) // Ladder 3
            {
                playerPosition = 86;
                System.out.println("\u001B[32m" + name + "you Found A Ladder!! GO UP!!! \u001B[0m");

            } else if (playerPosition == snakesLaddersArray[6]) // Magic tile 1
            {
                playerPosition = randInt(24,100);
                System.out.println("\033[33m" + name + "you Found A Magic Tile!! GO UP!!! \033[0m");

            } else if (playerPosition == snakesLaddersArray[7]) // Magic tile 2
            {
                playerPosition = randInt(64,100);
                System.out.println("\033[33m" + name + " you Found A Magic Tile!! GO UP!!! \033[0m");

            }

            pos = playerPosition;

        return pos;
    }


    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    private static String getInput() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Press (R) or (r) to roll the dice OR (s) to save and roll or (sq) to save and quit: ");
        String input = scanner.nextLine();

        return input;
    }
}
