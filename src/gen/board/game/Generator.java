package gen.board.game;

import org.json.simple.JSONObject;

import java.util.List;
import java.util.Scanner;

public class Generator {

// For testing reasons
    public void showGameData() {

        Selector selector = new Selector();


        try {
            Scanner scanner = new Scanner(System.in);
            String gameName = selector.promptGameName(scanner);
            JSONObject game = selector.extractGameInformation(gameName);
            int boardSize = 100;
            int minNumPlayers = selector.minNumPlayers(gameName, game);
            int maxNumPlayers = selector.maxNumPlayers(gameName, game);
            int numPlayersInput = selector.promptNumPlayers(scanner, maxNumPlayers);
            List<String> names = selector.promptPlayerNames(scanner, game, numPlayersInput);


            System.out.println("\n");
            System.out.println("\033[32mGame you have selected: \033[0m" + gameName);
//            System.out.println("\033[32mMinimum number of Players: \033[0m" + minNumPlayers);
//            System.out.println("\033[32mMaximum number of Players: \033[0m" + maxNumPlayers);
            System.out.println("\033[32mNumber of Players you selected: \033[0m" + numPlayersInput);
//            selector.printList(names);
            System.out.println();


            if (gameName.equals("snakes and ladders")) {
                GameSnakes snakes = new GameSnakes(names, boardSize);
                snakes.board();
            }
            if (gameName.equals("tic tac toe")) {
                System.out.println("\033[33m" + "Coming soon" + "\033[0m");
            } else {
                System.out.println("\033[33m" + "Good bye!!" + "\033[0m");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
