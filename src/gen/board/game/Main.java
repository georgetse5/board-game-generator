package gen.board.game;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("\033[36m" + "\n##########################" + "\033[0m");
        System.out.println("\033[36m" + "#  Board Game Generator  #" + "\033[0m");
        System.out.println("\033[36m" + "##########################\n" + "\033[0m");

        Generator generator = new Generator();
        generator.showGameData();

//        GameSnakes snakes = new GameSnakes();
//        snakes.mainLoop();
    }
}
