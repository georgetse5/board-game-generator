package gen.board.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Selector {

    private int maxNumPlayers, minNumPlayers, numPlayersInput;
    private String gameName, gameID;
    List<String> playerNames = new ArrayList<>();

// =================================================================================================================

    public String gameSelector() {

        Scanner input = new Scanner(System.in);
        String prompt;

        System.out.print("Choose the number or a keyword for one of the games above:\n(1) (snakes) and ladders\n(2) Coming Soon\n>> ");
        prompt = input.nextLine();

        while ((!prompt.equals("exit")) && (!prompt.equals("e"))) {

            if ((prompt.equals("1")) || (prompt.equals("snakes"))) {
                System.out.println("Snakes and ladders selected");
                break;
            }
            if (prompt.equals("2")) {
                System.out.println("Coming Soon!");
            } else {
                System.out.println("This is not in the list");
            }

            System.out.print("Choose the number for one of the games above:\n(1) (snakes) and ladders\n(2) Coming Soon\n>> ");
            prompt = input.nextLine();
        }

        return prompt;
    }

// =================================================================================================================
//        try {

    public String promptGameName(Scanner scanner) {

        System.out.print("Choose the number for one of the games above:\n" + "\033[33m(1)\033[0m Snakes and Ladders\n" + "\033[33m(2)\033[0m Tic Tac Toe\n" + "And \033[33m(exit)\033[0m or \033[33m(e)\033[0m to quit\n");
        System.out.print("Enter game's id: ");
        gameID = scanner.nextLine();

        while (true) {

            if (gameID.equals("1")) {
                gameName = "snakes and ladders";
                break;
            }

            if (gameID.equals("2")) {
                gameName = "tic tac toe";
                System.out.println("**" + gameName + "**");
                break;
            }

            if (gameID.equals("e") || gameID.equals("exit")) {
                System.out.println("Quiting...");
                System.out.println(":/");
                break;
            }

            System.out.println("\n");
            System.out.print("Choose the number for one of the games above:\n" + "\033[33m(1)\033[0m Snakes and Ladders\n" + "\033[33m(2)\033[0m Tic Tac Toe\n" + "And \033[33m(exit)\033[0m or \033[33m(e)\033[0m to quit\n");
            System.out.print("Enter game's id: ");
            gameID = scanner.nextLine();

        }

        return gameName;
    }



    public static JSONObject extractGameInformation(String gameName) throws Exception {

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("games.json"));
            JSONObject jsonObject = (JSONObject) obj;

            JSONArray boardGame = (JSONArray) jsonObject.get("boardGame");
            JSONObject game = null;
            for (int i = 0; i < boardGame.size(); i++) {
                JSONObject obj1 = (JSONObject) boardGame.get(i);
                if (obj1.get("gameTitle").equals(gameName)) {
                    game = obj1;
                    break;
                }
            }
            if (game == null) {
                System.out.println("Invalid game name");
                System.exit(0);
            }

        return game;
    }



    public boolean hasDice(String gameName) throws Exception {

        JSONObject game = extractGameInformation(gameName);

        return (Boolean) game.get("dice");
    }



    public int minNumPlayers(String gameName, JSONObject game) {

        minNumPlayers = ((Long) ((JSONObject) game.get("num_players")).get("min")).intValue();

        return minNumPlayers;
    }



    public int maxNumPlayers(String gameName, JSONObject game) {

            maxNumPlayers = ((Long) ((JSONObject) game.get("num_players")).get("max")).intValue();

        return maxNumPlayers;
    }



    public int promptNumPlayers(Scanner scanner, int numPlayers) {

        if (numPlayers > 2) {
            System.out.print("(Maximum players: " + maxNumPlayers + " and Minimum: " + minNumPlayers + "): ");
            numPlayersInput = scanner.nextInt();
        }
        if (numPlayers == 2) {
            numPlayersInput = 2;
        }
        if (numPlayersInput < 2 || numPlayersInput > numPlayers) {
            System.out.println("Invalid number of players");
            System.exit(0);
        }

        return numPlayersInput;
    }



    public List<String> promptPlayerNames(Scanner scanner, JSONObject game, int numPlayersInput) {

        JSONArray playersArr = (JSONArray) game.get("playerName");
        JSONObject playersObj = (JSONObject) playersArr.get(0);
        for (int i = 1; i <= numPlayersInput; i++) {
            String tempName = "";
            do {
                System.out.print("Enter name of player " + i + ": ");
                tempName = scanner.next();
                playerNames.add(tempName);
            } while (tempName.equals(""));
//            System.out.println("Player " + i + ": " + tempName);
        }
        return playerNames;
    }

    public void printList(List<String> playerNames) {
        int i = 1;
        System.out.println();
        for (String name : playerNames) {
            System.out.println("Player " + i + ": " + name);
            i = i + 1;
        }
    }


//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
